#0430_content_start
import urllib2, json
from datetime import datetime
date = datetime.now().strftime('%Y-%m-%d')
start = '0430'

body = json.dumps({"start_timecode":"16:05:00:00","title":date + '_' 'matt'})
request = urllib2.urlopen('http://localhost:65009/content_start', body)
response = json.loads(request.read())
assert response['error'] == 0
