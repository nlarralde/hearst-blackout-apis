#0430_content_start
import urllib2, json
from datetime import datetime
date = datetime.now().strftime('%Y-%m-%d')
start = '0500'

body = json.dumps({"start_timecode":"04:59:45:00","title":date + start})
request = urllib2.urlopen('http://localhost:65009/content_start', body)
response = json.loads(request.read())
assert response['error'] == 0
